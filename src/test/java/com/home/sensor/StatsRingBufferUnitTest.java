package com.home.sensor;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.home.Stats;
import com.home.StatsRingBuffer;

@RunWith(SpringRunner.class)
public class StatsRingBufferUnitTest {

	@Test
	public void testAverageInSameHourWhenSensorEventInSameHour() {
		StatsRingBuffer ringBuffer = new StatsRingBuffer(24);
		ringBuffer.put(23, 95, LocalDateTime.now().getDayOfYear());
		ringBuffer.put(23, 92, LocalDateTime.now().getDayOfYear());
		ringBuffer.put(23, 97, LocalDateTime.now().getDayOfYear());
		double actual = Stats.getRoundedAverage(ringBuffer.take(23).getTotal(), ringBuffer.take(23).getCount());
		double expected = 94.7;
		assertEquals(expected, actual, 0.1);
	}

	@Test
	public void testAverageInSameHourWhenSensorEventInDifferentHour() {
		StatsRingBuffer ringBuffer = new StatsRingBuffer(24);
		ringBuffer.put(0, 95, LocalDateTime.now().getDayOfYear());
		ringBuffer.put(15, 92, LocalDateTime.now().getDayOfYear());
		ringBuffer.put(23, 97, LocalDateTime.now().getDayOfYear());
		double actual = Stats.getRoundedAverage(ringBuffer.take(23).getTotal(), ringBuffer.take(23).getCount());
		double expected = 97;
		assertEquals(expected, actual, 0.1);
	}

}
