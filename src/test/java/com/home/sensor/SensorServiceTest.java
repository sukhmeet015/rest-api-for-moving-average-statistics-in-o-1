package com.home.sensor;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.home.sensors.SensorMetric;
import com.home.sensors.SensorService;

import javassist.tools.rmi.ObjectNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorServiceTest {

	@Autowired
	SensorService sensorService;


	@Test(expected = ObjectNotFoundException.class)
	public void testWhenInvalidSensorSendEvent() throws ObjectNotFoundException {
		String uuid = "sensor-9";
		sensorService.saveMeasurment(uuid, 95.8, LocalDateTime.now());
	}

	@Test()
	public void testSensorRegister() throws ObjectNotFoundException {
		String expected = "sensor-1";
		String actual = sensorService.registerSensor("sensor-1").getSensorUuid();
		assertEquals("sensor-1 not created", expected, actual);
	}

	@Test()
	public void checkSensorEventForIncreasingTemp() throws ObjectNotFoundException {
		String sensor_1 = "sensor-1";
		sensorService.registerSensor(sensor_1);
		sensorService.saveMeasurment(sensor_1, 96.8, LocalDateTime.now());
		sensorService.saveMeasurment(sensor_1, 97.0, LocalDateTime.now());
		sensorService.saveMeasurment(sensor_1, 98.8, LocalDateTime.now());
		int actualEventGenerated = sensorService.getEvents(sensor_1).size();
		int expectedEventGenerated = 1;
		assertEquals("Mismatch number with event", expectedEventGenerated, actualEventGenerated);
	}

	@Test()
	public void checkSensorEventForIncreasingDecreasingEvent() throws ObjectNotFoundException {
		String sensorId = "sensor-2";
		sensorService.registerSensor(sensorId);
		sensorService.saveMeasurment(sensorId, 96.8, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 97.0, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 92.8, LocalDateTime.now());
		int actualEventGenerated = sensorService.getEvents(sensorId).size();
		int expectedEventGenerated = 0;
		assertEquals("Mismatch number with event", expectedEventGenerated, actualEventGenerated);
		sensorService.saveMeasurment(sensorId, 96.8, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 97.0, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 99.8, LocalDateTime.now());
		actualEventGenerated = sensorService.getEvents(sensorId).size();
		expectedEventGenerated = 1;
		assertEquals("Mismatch number with event", expectedEventGenerated, actualEventGenerated);
	}

	@Test()
	public void checkSensorMetricForLastHourStats() throws ObjectNotFoundException {
		String sensorId = "sensor-3";
		sensorService.registerSensor(sensorId);
		sensorService.saveMeasurment(sensorId, 96.8, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 97.0, LocalDateTime.now());
		sensorService.saveMeasurment(sensorId, 92.8, LocalDateTime.now());
		SensorMetric actualMetric = sensorService.getMetric(sensorId, LocalDateTime.now());
		double last30DaysMaxActual = actualMetric.getMaxLast30Days();
		double last30DaysMaxExpected = 97.0;
		double lastHourAverageExpected = 95.5;
		double lastHourAverageActual = actualMetric.getAverageLastHour();
		double lastWeeklyExpected = 95.5;
		double lastWeeklyActual = actualMetric.getAverageLast7Days();
		assertEquals("Mismatch last hour average", String.valueOf(lastHourAverageExpected), String.valueOf(lastHourAverageActual));
		assertEquals("Mismatch last 7 days average", String.valueOf(lastWeeklyExpected), String.valueOf(lastWeeklyActual));
		assertEquals("Mismatch last 30 days max temperatur", String.valueOf(last30DaysMaxExpected), String.valueOf(last30DaysMaxActual));
	}

	@Test()
	public void checkSensorMetricForLastWeek() throws ObjectNotFoundException {
		String sensorId = "sensor-4";
		sensorService.registerSensor(sensorId);
		sensorService.saveMeasurment(sensorId, 92.8, LocalDateTime.now().minusDays(7));
		sensorService.saveMeasurment(sensorId, 96.8, LocalDateTime.now().minusDays(3));
		sensorService.saveMeasurment(sensorId, 97.0, LocalDateTime.now());
		SensorMetric actualMetric = sensorService.getMetric(sensorId, LocalDateTime.now());
		double last30DaysMaxActual = actualMetric.getMaxLast30Days();
		double last30DaysMaxExpected = 97.0;
		double lastHourAverageExpected = 97.0;
		double lastHourAverageActual = actualMetric.getAverageLastHour();
		double lastWeeklyExpected = 95.5;
		double lastWeeklyActual = actualMetric.getAverageLast7Days();
		assertEquals("Mismatch last hour average", String.valueOf(lastHourAverageExpected), String.valueOf(lastHourAverageActual));
		assertEquals("Mismatch last 7 days average", String.valueOf(lastWeeklyExpected), String.valueOf(lastWeeklyActual));
		assertEquals("Mismatch last 30 days max temperatur", String.valueOf(last30DaysMaxExpected), String.valueOf(last30DaysMaxActual));
	}

	@Test()
	public void checkSensorMetricForLast30daysMax() throws ObjectNotFoundException {
		String sensorId = "sensor-5";
		sensorService.registerSensor(sensorId);
		sensorService.saveMeasurment(sensorId, 97.0, LocalDateTime.now().minusDays(1));
		sensorService.saveMeasurment(sensorId, 96.8, LocalDateTime.now().minusDays(24));
		sensorService.saveMeasurment(sensorId, 92.8, LocalDateTime.now().minusDays(27));
		SensorMetric actualMetric = sensorService.getMetric(sensorId, LocalDateTime.now());
		double last30DaysMaxActual = actualMetric.getMaxLast30Days();
		double last30DaysMaxExpected = 97.0;
		double lastHourAverageExpected = 0;
		double lastHourAverageActual = actualMetric.getAverageLastHour();
		double lastWeeklyExpected = 97.0;
		double lastWeeklyActual = actualMetric.getAverageLast7Days();
		assertEquals("Mismatch last hour average", String.valueOf(lastHourAverageExpected), String.valueOf(lastHourAverageActual));
		assertEquals("Mismatch last 7 days average", String.valueOf(lastWeeklyExpected), String.valueOf(lastWeeklyActual));
		assertEquals("Mismatch last 30 days max temperatur", String.valueOf(last30DaysMaxExpected), String.valueOf(last30DaysMaxActual));
	}

}
