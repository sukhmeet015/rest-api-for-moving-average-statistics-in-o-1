package com.home;

public class Stats {
	double total;
	int count;
	double max;
	int day;

	public Stats() {
		super();
	}


	public Stats(double total, int count, double max, int day) {
		super();
		this.total = total;
		this.count = count;
		this.max = max;
		this.day = day;

	}

	public double getTotal() {
		return (double) Math.round(total * 100) / 100;
	}


	public int getDay() {
		return day;
	}

	public int getCount() {
		return count;
	}


	public double getMax() {
		return max;
	}

	public static double getRoundedAverage(double total, int count) {
		return Math.round((total / count) * 10.0) / 10.0;
	}


	@Override
	public String toString() {
		return "Stats [total=" + total + ", count=" + count + ", max=" + max + ", day=" + day + "]";
	}


}
