package com.home;

public class StatsRingBuffer {
	public Stats[] buffer;
	private int capacity;

	public StatsRingBuffer(int capacity) {
		super();
		buffer = new Stats[capacity];
		this.capacity = capacity;
	}

	public void put(int interval, double temperatur, int day) {
		if (buffer[interval] != null) {
			double total = buffer[interval].getTotal() + temperatur;
			int count = buffer[interval].getCount() + 1;
			double max = buffer[interval].getMax();

			if (max < temperatur) {
				max = temperatur;
			}
			Stats stats = new Stats(total, count, max, day);
			buffer[interval] = stats;
		} else {
			buffer[interval] = new Stats(temperatur, 1, temperatur, day);
		}
		if ((interval + 1) == capacity) {
			buffer[0] = null;
		} else {
			buffer[interval + 1] = null;
		}
	}

	public Stats take(int interval) {
		if (buffer[interval] != null) {
			return buffer[interval];
		} else {
			return null;
		}
	}
}
