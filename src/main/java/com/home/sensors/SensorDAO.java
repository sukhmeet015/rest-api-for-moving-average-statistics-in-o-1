package com.home.sensors;

import java.util.HashMap;
import java.util.Map;

import org.javatuples.Triplet;
import org.springframework.stereotype.Component;

import com.home.StatsRingBuffer;

@Component
public class SensorDAO {
	Map<String, Triplet<StatsRingBuffer, StatsRingBuffer, StatsRingBuffer>> statsMap = new HashMap<>();


	public Triplet<StatsRingBuffer, StatsRingBuffer, StatsRingBuffer> getStatsMap(String uuid) {
		if (statsMap.get(uuid) == null) {
			StatsRingBuffer hourlyRingBuffer = new StatsRingBuffer(24);
			StatsRingBuffer weeklyRingBuffer = new StatsRingBuffer(7);
			StatsRingBuffer thirtyDayRingBuffer = new StatsRingBuffer(30);
			statsMap.put(uuid, new Triplet<StatsRingBuffer, StatsRingBuffer,
					StatsRingBuffer>(hourlyRingBuffer, weeklyRingBuffer, thirtyDayRingBuffer));
		}
		return statsMap.get(uuid);
	}


}
