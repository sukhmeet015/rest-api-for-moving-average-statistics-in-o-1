package com.home.sensors;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping("/sensors")
public class SensorController {
	@Autowired
	SensorService sensorService;

	@PutMapping("/{uuid}/register")
	@ApiOperation(value = "Sensor must be registered before sending temperature details")
	ResponseEntity<?> registerSensor(@PathVariable String uuid) {
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{uuid}").buildAndExpand(sensorService.registerSensor(uuid).getSensorUuid())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PostMapping("/{uuid}/measurment")
	@ApiOperation(value = "store temperature under sensor if sensor is registered")
	ResponseEntity<?> saveMeasurment(@PathVariable String uuid, @RequestBody double temperature) throws ObjectNotFoundException {
		sensorService.saveMeasurment(uuid, temperature, LocalDateTime.now());
		return ResponseEntity.status(201).build();
	}

	@GetMapping("/{uuid}/metrics")
	@ApiOperation(value = "returns metrics of average lasthour, last7days and max in last 30 days")
	ResponseEntity<SensorMetric> getMetrics(@PathVariable String uuid) {
		return ResponseEntity.ok().body(sensorService.getMetric(uuid, LocalDateTime.now()));
	}

	@GetMapping("/{uuid}/events")
	@ApiOperation(value = "returns event TEMPERATURE_EXCEEDED details")
	ResponseEntity<List<SensorEvent>> getEvents(@PathVariable String uuid) {

		return ResponseEntity.ok().body(sensorService.getEvents(uuid));
	}

}
