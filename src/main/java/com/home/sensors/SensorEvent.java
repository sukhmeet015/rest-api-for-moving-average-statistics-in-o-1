package com.home.sensors;

import java.time.LocalDateTime;

public class SensorEvent extends Sensor
{

	private static final long serialVersionUID = 1L;
	private SensorEventEnum type;
	private LocalDateTime at;
	private double temperature;

	public SensorEventEnum getType() {
		return type;
	}

	public SensorEvent(SensorEventEnum type, LocalDateTime at, double temperature) {
		super();
		this.type = type;
		this.at = at;
		this.temperature = temperature;
	}

	public void setType(SensorEventEnum type) {
		this.type = type;
	}

	public LocalDateTime getAt() {
		return at;
	}

	public void setAt(LocalDateTime at) {
		this.at = at;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
}
