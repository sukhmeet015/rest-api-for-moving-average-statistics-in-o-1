package com.home.sensors;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.home.Stats;
import com.home.StatsRingBuffer;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class SensorService {

	@Autowired
	SensorDAO sensorDAO;
	Map<String, Sensor> sensorMap = new HashMap<String, Sensor>();
	Map<String, Pair<Double, Integer>> sensorTemperatureCounter = new HashMap<String, Pair<Double, Integer>>();
	Map<String, List<SensorEvent>> sensorEvents = new HashMap<String, List<SensorEvent>>();
	static int id = 0;
	int count = 0;

	public Sensor registerSensor(String uuid) {
		if (sensorMap.get(uuid) == null) {
			sensorMap.put(uuid, new Sensor(uuid));
		}
		return sensorMap.get(uuid);
	}


	public void saveMeasurment(String uuid, Double temperature, LocalDateTime currentTime) throws ObjectNotFoundException {
		if (!sensorMap.containsKey(uuid)) {
			throw new ObjectNotFoundException("sensor id " + uuid + " not present");
		}
		checkTemperatureLimit(uuid, temperature);
		if (sensorTemperatureCounter.get(uuid).getValue1() >= 3) {
			createTemperatureExceededEvent(uuid, temperature);
		}
		saveStats(uuid, currentTime, temperature);
	}

	private void saveStats(String uuid, LocalDateTime reqDateTime, double temperature) {
		int hour = reqDateTime.getHour();
		int day = reqDateTime.getDayOfWeek().getValue();
		int dayNumberInYear = reqDateTime.getDayOfYear();
		if (reqDateTime.isAfter(LocalDateTime.now().minusHours(2))) {
			saveHourlyStats(hour, temperature, sensorDAO.getStatsMap(uuid).getValue0(), dayNumberInYear);
		}
		if (reqDateTime.isAfter(LocalDateTime.now().minusDays(8))) {
			saveWeeklyStats(day, temperature, sensorDAO.getStatsMap(uuid).getValue1(), dayNumberInYear);
		}
		if (reqDateTime.isAfter(LocalDateTime.now().minusDays(31))) {
			saveLast30DaysStats(compute30dayValue(dayNumberInYear), temperature, sensorDAO.getStatsMap(uuid).getValue2(), dayNumberInYear);
		}
	}

	private int compute30dayValue(int dayNumberInYear) {
		while (dayNumberInYear > 30) {
			dayNumberInYear = dayNumberInYear - 30;
		}
		return dayNumberInYear;
	}

	private void saveHourlyStats(int interval, double temperature, StatsRingBuffer hourlyStats, int dayNumberInYear) {
		hourlyStats.put(interval, temperature, dayNumberInYear);
	}

	private void saveWeeklyStats(int interval, double temperature, StatsRingBuffer weeklyStats, int dayNumberInYear) {
		weeklyStats.put(interval, temperature, dayNumberInYear);
	}

	private void saveLast30DaysStats(int interval, double temperature, StatsRingBuffer last30DaysStats, int dayNumberInYear) {
		last30DaysStats.put(interval, temperature, dayNumberInYear);
	}

	private void checkTemperatureLimit(String uuid, Double temperature) {
		if (sensorTemperatureCounter.get(uuid) == null) {
			if (temperature > 95) {
				sensorTemperatureCounter.put(uuid, new Pair<Double, Integer>(temperature, 1));
			} else {
				sensorTemperatureCounter.put(uuid, new Pair<Double, Integer>(temperature, 0));
			}

		} else {
			if (temperature > 95) {
				sensorTemperatureCounter.put(uuid, new Pair<Double, Integer>(temperature, sensorTemperatureCounter.get(uuid).getValue1() + 1));
			} else {
				sensorTemperatureCounter.put(uuid, new Pair<Double, Integer>(temperature, 0));
			}
		}
	}

	private void createTemperatureExceededEvent(String uuid, Double temperature) {
		sensorTemperatureCounter.put(uuid, new Pair<Double, Integer>(temperature, 0));
		SensorEvent event = new SensorEvent(SensorEventEnum.TEMPERATURE_EXCEEDED, LocalDateTime.now(), temperature);
		List<SensorEvent> eSensorEvents = sensorEvents.get(uuid);
		if (eSensorEvents == null) {
			eSensorEvents = new ArrayList<>();
		}
		eSensorEvents.add(event);
		sensorEvents.put(uuid, eSensorEvents);
	}

	public List<SensorEvent> getEvents(String uuid) {
		return sensorEvents.getOrDefault(uuid, new ArrayList<SensorEvent>());

	}

	public SensorMetric getMetric(String uuid, LocalDateTime currentTime) {
		SensorMetric sensorMetric  = new SensorMetric();
		int lastHour = currentTime.getHour();
		sensorMetric.setSensorUuid(uuid);
		sensorMetric.setAverageLastHour(computeHourlyStats(sensorDAO.getStatsMap(uuid).getValue0().take(lastHour), currentTime.getDayOfYear()));
		sensorMetric.setAverageLast7Days(computeDaysStats(sensorDAO.getStatsMap(uuid).getValue1(), 7, currentTime.getDayOfYear()));
		sensorMetric.setMaxLast30Days(computeMaxTemperature(sensorDAO.getStatsMap(uuid).getValue2(), 30, currentTime.getDayOfYear()));
		return sensorMetric;
	}

	private double computeHourlyStats(Stats stat, int dayOfYear) {
		if (stat != null) {
			if (dayOfYear == stat.getDay()) {
				return Stats.getRoundedAverage(stat.getTotal(), stat.getCount());
			}
		}
		return 0;
	}

	private double computeDaysStats(StatsRingBuffer value1, int capacity, int dayOfYear) {
		double totalTemp = 0;
		int count = 0;
		for (int i = 0; i < capacity; i++) {
			if (value1.take(i) != null) {
				if ((dayOfYear - value1.take(i).getDay()) <= capacity) {
					totalTemp += value1.take(i).getTotal();
					count += value1.take(i).getCount();
				}
			}
		}
		if (count == 0) {
			return 0;
		}
		return Stats.getRoundedAverage(totalTemp, count);
	}

	private double computeMaxTemperature(StatsRingBuffer value1, int capacity, int dayOfYear) {
		double maxTemperatur = 0;
		for (int i = 0; i < capacity; i++) {
			if ((value1.take(i) != null) && (value1.take(i).getMax() > maxTemperatur)) {
				if ((dayOfYear - value1.take(i).getDay()) < capacity) {
					maxTemperatur = value1.take(i).getMax();
				}
			}
		}
		return maxTemperatur;
	}


}
