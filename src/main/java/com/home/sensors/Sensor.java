package com.home.sensors;

import java.io.Serializable;

public class Sensor implements Serializable {

	private static final long serialVersionUID = 1L;
	private String sensorUuid;

	public Sensor() {
		super();
	}

	public Sensor(String sensorUuid) {
		super();
		this.sensorUuid = sensorUuid;
	}

	public String getSensorUuid() {
		return sensorUuid;
	}


}
