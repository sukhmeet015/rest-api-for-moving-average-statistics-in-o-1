package com.home.sensors;

import java.io.Serializable;

public class SensorMetric implements Serializable {
	private static final long serialVersionUID = -2408797636042886087L;
	private String sensorUuid;
	private double averageLastHour;
	private double averageLast7Days;
	private double maxLast30Days;
	public String getSensorUuid() {
		return sensorUuid;
	}
	public void setSensorUuid(String sensorUuid) {
		this.sensorUuid = sensorUuid;
	}

	public double getAverageLastHour() {
		return averageLastHour;
	}

	public void setAverageLastHour(double averageLastHour) {
		this.averageLastHour = averageLastHour;
	}

	public double getAverageLast7Days() {
		return averageLast7Days;
	}

	public void setAverageLast7Days(double averageLast7Days) {
		this.averageLast7Days = averageLast7Days;
	}

	public double getMaxLast30Days() {
		return maxLast30Days;
	}

	public void setMaxLast30Days(double maxLast30Days) {
		this.maxLast30Days = maxLast30Days;
	}


}
