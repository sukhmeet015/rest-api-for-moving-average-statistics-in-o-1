
Sensor Application:

I have created a restful api in spring which register sensors emitting temperature, recoreds temperature events
and when it exceeds temperature continuosuly three times it will emit temperature exceeded event which can be used as alarming event.

There is an end points for statistics which gives statistics/ moving average for last hourly, 7, 30 days and 
also max temperature in last 30 days, with optimistic O(1) Time complexity.

I have used swagger api documentation.


Steps to run application: 
clone the project using "git clone https://sukhmeet015@bitbucket.org/sukhmeet015/rest-api-for-moving-average-statistics-in-o-1.git"

•	All endpoints are implemented with O(1).
•	Restful api for sensor emiting temperature details, registering itself and creating events with average statistics


Approach 1:

	1.	This is maven project, import the project in eclipse as maven project
	2.	Run maven update on the project to load all dependencies from central repo
	3.	After all compilation resolved, run project as spring boot application 
	4.	After you have ran the project there is swagger configured to test api endpoints 
	5.	http://localhost:8080/swagger-ui.html#/ 
	6.	First use register endpoint to register sensor before storing measurement
	7.	Then use post measurement endpoint to record temperature of sensor
	8.	Use metric and events endpoint respective accordingly.
	
Approach 2:

	1.	Go to project directory through cmd
	2.	Run mvn install or mvn package
	3.	After success, this will produce jar file in target folder
	4.	Go to target folder
	5.	Run java –jar “your-jar.jar”
	6.	This will run application on embedded tomcat run by spring-boot.
	7.	Go to web browser and use http://localhost:8080/swagger-ui.html#/.
	


Scalability:


•	This algorithm to calculate moving average is not limited to 7 or 30 days

o	You can easily compute last n days average as well as max cause I have used circular array also acting as a ring buffer to store value and size will be same as capacity of statistics required.



Limitation:

•	Rate limiting can be implemented in case sensor/device sends faulty temperature or pushes unessary temparature event.

•	As I have not used permanent storage, data will be lost once application stops

•	There is slight difference in computeHourlyStats and computeDaysStats methods but the core is same i.e. underlying buffer. However computeDaysStats 
    can be used for all evening hourslyStats.
	
•	I have not used mocking as I am testing core part of buffer as unit with unit test and service as integration test, however I can use it for other persons





Datastructure used:
1. Arrya as RingBuffer/circular buffer
2. Map 
3. Tupple
